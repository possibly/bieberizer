tell application "Finder"
	set myfolder to parent of (path to me) as string
	set myfolder to POSIX path of myfolder
	set app_path to myfolder
end tell

tell application "Finder"
	set app_path to app_path & "thebieb.jpg"
	set desktop picture to POSIX file app_path
end tell